<?php
namespace MyProject\Kernel;;

use Exception;

class Session
{
    private static $default_values = [];
    public static function setDefaultValues($default_values = [])
    {
        self::$default_values = $default_values;
    }
    private static function loadDefaultValues($hard = false)
    {
        if ($hard) {
            $_SESSION = self::$default_values;
        } else {
            foreach(self::$default_values as $key => $value)
            {
                $_SESSION[$key] = $value;
            }
        }
        $_SESSION['user_classname'] = self::$user_classname;
        $_SESSION['profile_classname'] = self::$profile_classname;
        $_SESSION['privilege_classname'] = self::$privilege_classname;
    }

    private static $user_classname = null;
    public static function setUserClassname($classname)
    {
        self::$user_classname = $classname;
    }

    private static $profile_classname = null;
    public static function setProfileClassname($classname)
    {
        self::$profile_classname = $classname;
    }

    private static $privilege_classname = null;
    public static function setPrivilegeClassname($classname)
    {
        self::$privilege_classname = $classname;
    }

    public static function reboot()
    {
        self::unset();
        self::start();
    }

    public static function start()
    {
        session_start();
        self::reset();
        self::checkUserSession();
    }

    private static function reset()
    {
        // loading default values
        $empty = empty($_SESSION);
        self::loadDefaultValues($empty);
    }

    public static function unset()
    {
        session_unset();
        self::reset();
    }

    public static function get($name)
    {
        if (!isset($_SESSION[$name]))
        {
            throw new Exception("Undefined Session variable `{$name}`.");
        }
        return $_SESSION[$name];
    }

    public static function set($name, $value)
    {
        $_SESSION[$name] = $value;
    }

    private static function checkUserSession()
    {
        if(! isset($_SESSION['connected'])) {
            throw new Exception("Session variable `connected` not defined, please use setDefaultValues to define it.");
        } elseif (! isset($_SESSION['user_classname'])) {
            throw new Exception("Session user classname not defined, please use setUserClassname to define it.");
        } elseif (! method_exists($_SESSION['user_classname'], "get")) {
            throw new Exception("Method `get` not found in class `{$_SESSION['user_classname']}`.");
        } elseif (! isset($_SESSION['profile_classname'])) {
            throw new Exception("Session profile classname not defined, please use setProfileClassname to define it.");
        } elseif (! method_exists($_SESSION['profile_classname'], "getAll")) {
            throw new Exception("Method `getAll` not found in class `{$_SESSION['profile_classname']}`.");
        } elseif (! isset($_SESSION['privilege_classname'])) {
            throw new Exception("Session privilege classname not defined, please use setPrivilegeClassname to define it.");
        } elseif (! method_exists($_SESSION['privilege_classname'], "getAll")) {
            throw new Exception("Method `getAll` not found in class `{$_SESSION['privilege_classname']}`.");
        } else {
            if ($_SESSION['connected']) {
                // user is connected
                // get user info
                $user_classname = $_SESSION['user_classname'];
                $profile_classname = $_SESSION['profile_classname'];
                $privilege_classname = $_SESSION['privilege_classname'];
        
                // Checking login and password
                $login = $_SESSION['user']->login;
                $password = $_SESSION['user']->password;
    
                $user = $user_classname::get($login, $password);
    
                if ($user == null)
                {
                    // Invalid Session
                    self::unset();
                    $_SESSION['errors'][] = "Invalid session." ;
                }
                else
                {
                    // Updating Session
                    $_SESSION['user'] = $user;
                    $_SESSION['profiles'] = $profile_classname::getAll($user->id);
                    $_SESSION['privileges'] = $privilege_classname::getAll($user->id);
                }
            }
        }
    }

    public static function is_assigned($profile)
    {
        return self::property_name_in_array(
            $profile, 
            $_SESSION['profiles']
        );
    }

    public static function is_granted($privilege)
    {
        return self::property_name_in_array(
            $privilege, 
            $_SESSION['privileges']
        );
    }

    public static function property_name_in_array($name, $array)
    {
        $result = false;
        $count = count($array);
        for($i = 0; $i < $count && ! $result; $i++)
        {
            $result = $name == $array[$i]->name;
        }
        return $result;
    }
}
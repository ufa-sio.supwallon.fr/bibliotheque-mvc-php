<?php
namespace MyProject\Kernel;
 
final class Router
{
    private static $rootPath = null;
    private static $uri = null;
    private $routes;

    /**
     * __construct
     */
    public function __construct()
    {
        $this->routes = array();
        $this->initUri();
    }

    /**
     * Get the path of the index.php
     * @return string
     */
    public static function getRootPath() : string
    {
        if (! isset(self::$rootPath))
        {
            self::$rootPath = implode('/', array_slice(explode('/', $_SERVER['SCRIPT_NAME']), 0, -1));
        }
        return self::$rootPath;
    }


    /**
     * Initlialize the Uri
     * @return void
     */
    private static function initUri()
    {
        $path = self::getRootPath();
        $uri = substr($_SERVER['REQUEST_URI'], strlen($path .'/'));
        if (strstr($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        self::$uri = '/' . trim($uri, '/');
    }

    /**
     * Get the Uri
     * @return string
     */
    public function getUri() : string
    {
        if (! isset(self::$uri))
        {
            self::initUri();
        }
        return self::$uri;
    }

    /**
     * Function getRoutes
     *
     * @return array Route Collection
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Procedure addRoute
     * 
     * @param object $route instanceof Route
     *
     * @return void
     */
    public function addRoute($route)
    {
        array_push($this->routes, $route);
    }

    /**
     * Function findRoute
     *
     * @return object instanceof Route
     */
    public function findRoute()
    {
        $route = array_shift($this->routes);
        while ($route != null && !$route->match(self::$uri)) {
            $route = array_shift($this->routes);
        }
        return $route;
    }
}

<?php
namespace MyProject\Kernel;

class Error
{
    public $code;
    public $message;
    public $details;
    public $type;

    /**
     * Constructor
     * @param string $message
     * @param enum $type
     * @param string $details
     * @param int $code
     */
    public function __construct($message = "Error!", $type = "warning", $details = "", $code = 0)
    {
        $this->message = $message;
        $this->type = $type;
        $this->details = $details;
        $this->code = $code;
    }
}
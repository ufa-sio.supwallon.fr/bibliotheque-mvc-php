<?php

namespace MyProject\Kernel;

use PDO;
use PDOException;

/**
 * Kernel Database class
 */
class Database
{
    private static $host = "127.0.0.1";
    private static $port = "3306";
    private static $dbname = "database";
    private static $charset = "UTF8";
    private static $user = "user";
    private static $password = "p4sSw0r7";
    private static $dbh;

    /**
     * Initialize the database configuration
     * @param array $configuration
     */
    public static function init(array $configuration = []) : void
    {
        foreach($configuration as $key => $value)
        {
            self::$$key = $value;
        }
    }

    /**
     * Open new connection to the database
     * @return PDO PDO instance of the connection
     */
    public static function open() : PDO
    {
        $dsn = "mysql:" .
                "host=" . self::$host . ";" .
                "port=" . self::$port . ";" .
                "dbname=" . self::$dbname . ";" . 
                "charset=" . self::$charset . ";";
        
        try {
            self::$dbh = new PDO($dsn, self::$user, self::$password);
            return self::$dbh;
        } catch (PDOException $ex) {
            View::setTemplate("error.tpl");
            View::bindVar("type", "danger");
            View::bindVar("code", "1");
            View::bindVar("message", "Fatal Error: Database connection failed.");

            View::display();
            die();
        }
    }

    public static function close(PDO &$dbh = null)
    {
        $dbh = null;
        self::$dbh = null;
    }
}
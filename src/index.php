<?php
namespace MyProject;

require_once "vendor/autoload.php";

use MyProject\Kernel\Database;
use MyProject\Kernel\Route;
use MyProject\Kernel\Router;
use MyProject\Kernel\Session;

// Initialize Database Configuration
Database::init([
    "host" => "127.0.0.1",
    "port" => "3306",
    "dbname" => "database",
    "charset" => "UTF8",
    "user" => "user",
    "password" => "p4sSw0r7",
]);
// Initialize Session Manager
Session::setDefaultValues([
    "connected" => false,
    "errors" => [],
]);
Session::setUserClassname("Utilisateur");
Session::setProfileClassname("Groupe");
Session::setPrivilegeClassname("Droit");
Session::start();

$router = new Router();
$router->addRoute(new Route("/", "MyProject\\Controllers\\HomeController"));
$router->addRoute(new Route("/error/{*}", "MyProject\\Controllers\\HomeController"));

$route = $router->findRoute();

if ($route)
{
    $route->execute();
}
else
{
    // 404 Error
    $path = Router::getRootPath();
    $route = "/error/404";
    header("location: {$path}{$route}");
}
<?php
namespace MyProject\Controllers;

use MyProject\Kernel\Error;
use MyProject\Kernel\IController;
use MyProject\Kernel\Route;
use MyProject\Kernel\Router;
use MyProject\Kernel\View;

class HomeController implements IController
{
    public static function route()
    {
        // Routage secondaire
        $router = new Router();
        $router->addRoute(new Route("/", "MyProject\\Controllers\\HomeController", "homeAction"));
        $router->addRoute(new Route("/error/{code}", "MyProject\\Controllers\\HomeController", "errorAction"));

        $route = $router->findRoute();

        if ($route)
        {
            $route->execute();
        }
        else
        {
            // 404 Error
            $path = Router::getRootPath();
            $route = "/error/404";
            header("location: {$path}{$route}");
        }
    }

    public static function homeAction()
    {
        View::setTemplate("home.tpl");
        View::display();
    }

    public static function errorAction($code)
    {
        switch($code)
        {
            case '404':
                $error = new Error("Page not found.", "warning", "", $code);
                break;
            default:
                $error = new Error("Unknown error.", "warning", "", $code);
                break;
        }

        View::setTemplate("error.tpl");
        View::bindVar("message", $error->message);
        View::bindVar("type", $error->type);
        View::bindVar("code", $error->code);
        View::display();
    }
}
            <form class="form-inline" role="form" action="<?= $path ?>/user/logout" method="post">
                <span class="oi oi-person mr-2"></span><?= $login; ?>
                <button class="btn btn-outline-secondary ml-2 my-2 my-sm-0" type="submit" aria-label="Logout" title="Logout"><span class="oi oi-account-logout"></span></button>
            </form>

        <script src="<?= $path; ?>/vendor/components/jquery/jquery.min.js" crossorigin="anonymous"></script>
        <script src="<?= $path; ?>/vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<?php
$js_dir = "src/View/js/";
$files = scandir($js_dir);
foreach($files as $filename):
$file_parts = explode(".", $filename);
$ext = array_pop($file_parts);
if(is_file($js_dir . $filename) && $ext == "js"):
?>        <script src="<?= $path . "/" . $js_dir . $filename; ?>" crossorigin="anonymous"></script>
<?php endif; ?>
<?php endforeach; ?>

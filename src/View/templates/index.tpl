<!DOCTYPE html>
<html lang="en">
<?php require_once "head.tpl"; ?>
    <body>
<?php require_once "navbar.tpl"; ?>
        <main role="main" class="container">
<?php require_once "alert.tpl"; ?>
<?php require_once $template; ?>
        </main>
<?php require_once "scripts.tpl"; ?>
    </body>
</html>
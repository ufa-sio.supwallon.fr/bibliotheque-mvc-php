        <div class="container">
            <div class="alert alert-<?= $type ?>" role="alert">
            <h4 class="alert-heading">Error <?= $code ?></h4>
            <p><?= $message ?></p>
<?php if(isset($details)): ?>
            <hr>
            <p class="mb-0"><?= $details ?></p>
<?php endif; ?>
            </div>
        </div>
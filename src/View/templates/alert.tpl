<?php if(isset($errors)): ?>
<?php foreach($errors as $error): ?>
            <div class="alert alert-<?= $error->type ?> alert-dismissible fade show row" role="alert">
                <div class="col-sm-1"><?= $error->code ?></div>
                <div class="col-sm"><?= $error->message ?></div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
<?php endforeach; ?>
<?php endif; ?>

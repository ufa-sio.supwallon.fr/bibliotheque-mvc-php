# Tiny MVC 2

Tiny PHP MVC Framework

## MVC Implementation

### Model

#### _Data Abstract Layer_ (Dal)

Tiny MVC 2 do not use a real _DAL_

The Abstraction Layer only provide the class __Database__ that is a centralized database connection.

The __Database__ class define the connection parameters, merge them into the _dsn_ (Data Source Name) and use __PDO__ to manage the database connection.

#### Data Access Object (Dao)

The __Dao__ get the __PDO__ instance provided by the __Database__ class.

These classes are used to :

- __define__ the SQL statement
- __prepare__ the statement
- __parametrize__ the statement using method parameters
- __execute__ the statement
- __Instance__ and __hydrate__ the *Entities*

#### Entities

The entities are the classes used by the web application that reflects database informations.
Entities inherits from the kernel __Entity__ class that enables getters and setters functionalities.
Entities can also use the kernel __Dictionary__ class for One to Many links.

### View

#### Templates

### Controllers

## URL Routing

### URL Rewriting

### Primary Routing

### Secondary Routing

## Session Management

### Session Variables

### User Profiles and Privileges

For more details see [CONTRIBUTING.md](CONTRIBUTING.md)
